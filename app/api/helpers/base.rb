# frozen_string_literal: true

module Helpers
  module Base
    def permitted_params
      @permitted_params ||= declared(params, include_missing: false)
    end

    def logger
      Rails.logger
    end

    def authenticate!
      error!('Invalid or expired token.', 401) unless current_user
    end

    def current_user
      @current_user ||= begin
        error!('Missing authorization header.', 401) unless http_auth_header
        api_key = ::ApiKeys::FromToken.call(token: http_auth_header)

        if api_key.present? && !api_key.expired?
          api_key.user
        end
      end
    end

    def http_auth_header
      headers['Authorization'] || params['api_key']
    end
  end
end
