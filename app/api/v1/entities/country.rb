# frozen_string_literal: true

module V1
  module Entities
    class Country < Base
      expose :alpha2
      expose :alpha3
      expose :emoji_flag
    end
  end
end
