# frozen_string_literal: true

module V1
  module Resources
    class Users < Base
      include V1::Authenticated

      resource :users do
        get do
          users = User.all

          present users, with: Entities::User
        end

        params do
          requires :email, type: String
          requires :password, type: String
          requires :password_confirmation, type: String
          requires :name, type: String
        end
        post do
          user = User.create!(permitted_params)

          present user, with: Entities::User
        end

        route_param :user_id, type: Integer do
          get do
            user = User.find(permitted_params[:user_id])

            present user, with: Entities::User
          end

          params do
            optional :email, type: String
            requires :password, type: String
            optional :new_password, type: String
            optional :password_confirmation, type: String
            optional :name, type: String
          end
          patch do
            user = User.find(permitted_params[:user_id])

            attrs = permitted_params.except(:user_id)
            user.update!(attrs)

            present user, with: Entities::User
          end

          delete do
            user = User.find(permitted_params[:user_id])

            user.destroy!

            status 204
          end
        end
      end
    end
  end
end
