# frozen_string_literal: true

module V1
  module Resources
    class Auth < Base
      resource :auth do
        params do
          requires :email, type: String
          requires :password, type: String
          requires :name, type: String
        end
        post :register do
          user = ::Users::Register.call(params: permitted_params)

          present user, with: Entities::User, with_api_key: true
        end

        params do
          requires :email, type: String, desc: "Email address"
          requires :password, type: String, desc: "Password"
        end
        post :login do
          user = User.find_by(email: permitted_params[:email].downcase)

          error!("Unauthorized", 401) if user.nil?

          error!(:Unauthorized, 401) unless user.authenticate(params[:password])

          api_key = ::ApiKeys::FromUser.call(user_id: user.id)

          present api_key, with: Entities::ApiKey
        end

        post :logout do
          api_key = ApiKey.find_by!(access_token: http_auth_header)

          ::ApiKeys::Expire.call(api_key: api_key) unless api_key.expired?

          status 200
        end
      end
    end
  end
end
