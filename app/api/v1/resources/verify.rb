# frozen_string_literal: true

module V1
  module Resources
    class Verify < Base
      include V1::Authenticated

      get :verify do
        present current_user, with: Entities::User
      end
    end
  end
end
