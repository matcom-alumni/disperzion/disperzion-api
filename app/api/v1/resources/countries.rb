# frozen_string_literal: true

module V1
  module Resources
    class Countries < Base
      include V1::Authenticated

      resource :countries do
        get do
          countries = ISO3166::Country.countries

          present countries, with: Entities::Country
        end
      end
    end
  end
end
