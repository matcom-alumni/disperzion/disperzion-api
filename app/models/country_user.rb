# frozen_string_literal: true

class CountryUser < ApplicationRecord
  COUNTRY_CODES = ISO3166::Country.countries.map(&:country_code).freeze

  belongs_to :user

  validates :country_code, presence: true, inclusion: { in: COUNTRY_CODES }
end
