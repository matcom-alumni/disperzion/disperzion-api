# frozen_string_literal: true

module ApiKeys
  class Expire
    ACCESS_TOKEN_NAMESPACE = 'access_tokens'
    attr_reader :api_key, :expires_at

    def initialize(api_key:, at: Time.now)
      @api_key = api_key
      @expires_at = at
    end

    def self.call(*args)
      new(*args).call
    end

    def call
      api_key.update!(expires_at: expires_at)
      delete_cache
      api_key
    end

    private

    def delete_cache
      Rails.cache.delete("access_tokens/#{api_key.access_token}", namespace: ACCESS_TOKEN_NAMESPACE)
    end
  end
end
