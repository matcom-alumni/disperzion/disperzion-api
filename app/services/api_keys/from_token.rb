# frozen_string_literal: true

module ApiKeys
  class FromToken
    CACHE_EXPIRATION_SECONDS = 120
    ACCESS_TOKEN_NAMESPACE = 'access_tokens'

    attr_reader :token

    def initialize(token:)
      @token = token
    end

    def self.call(*args)
      new(*args).call
    end

    def call
      return if token.nil?

      Rails.cache.fetch("access_tokens/#{token}",
                        expires_in: CACHE_EXPIRATION_SECONDS,
                        namespace: ACCESS_TOKEN_NAMESPACE) do
        ApiKey.joins(:user).find_by(access_token: token)
      end
    end
  end
end
