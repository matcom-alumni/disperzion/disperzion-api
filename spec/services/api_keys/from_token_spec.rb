# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApiKeys::FromToken do
  subject { described_class.call(token: token) }

  let(:api_key) { create(:api_key) }

  context 'when token exists' do
    let(:token) { api_key.access_token }

    it 'returns the linked API Key' do
      is_expected.to eq(api_key)
    end

    it 'creates a new key in redis' do
      expect { subject }.to change { redis_keys.count }.by(1)
    end
  end

  context 'when token does not exists' do
    let(:token) { nil }

    it 'returns nil' do
      is_expected.to be_nil
    end

    it 'does not create a new key in redis' do
      expect { subject }.to change { redis_keys.count }.by(0)
    end
  end
end
