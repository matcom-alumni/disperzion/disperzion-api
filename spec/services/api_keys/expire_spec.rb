# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApiKeys::Expire do
  subject { described_class.call(api_key: api_key, at: expires_at) }

  let(:api_key) { create(:api_key) }
  let(:expires_at) { Time.now }

  it 'expires the api_key' do
    expect {
      subject
    }.to change(api_key, :expired?)
           .from(false).to(true)
  end

  context 'when access_token is cached' do
    let(:token) { api_key.access_token }

    before do
      Rails.cache.fetch("access_tokens/#{token}",
                        expires_in: 120,
                        namespace: 'access_tokens') do
        api_key
      end
    end

    it 'remove key from redis' do
      expect { subject }.to change { redis_keys.count }.by(-1)
    end
  end
end
