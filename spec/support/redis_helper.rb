# frozen_string_literal: true

module RedisHelper
  def redis_keys
    Rails.cache.redis.keys
  end
end
