# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Countries API V1' do
  let(:base_url) { '/v1/countries' }
  let(:country_type) do
    {
      alpha2: :string_or_null,
      alpha3: :string_or_null,
      emoji_flag: :string
    }
  end

  let(:api_key) { create(:api_key) }
  let(:headers) { { 'Authorization' => api_key.access_token } }

  describe 'GET /countries' do
    before { get base_url, headers: headers }

    it 'returns all countries' do
      expect_json_sizes(249)
    end

    it 'returns status code 200' do
      expect_status(200)
    end

    it 'returns right fields' do
      expect_json_types('*', country_type)
    end
  end
end
