# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Auth API V1' do
  let(:base_url) { '/v1/auth' }
  let(:api_key_type) do
    {
      id: :integer,
      access_token: :string,
      expires_at: :date,
      user_id: :integer,
      active: :boolean
    }
  end

  describe 'POST v1/auth/register' do
    let(:user_type) do
      {
        id: :integer,
        email: :string,
        name: :string,
        api_key: api_key_type
      }
    end
    let(:params) do
      {
        email: 'pepe@gmail.com',
        password: 'supersecretpassword',
        name: 'Pepe'
      }
    end

    before { post "#{base_url}/register", params: params }

    it 'returns 201 OK' do
      expect_status(201)
    end

    it 'returns correct types' do
      expect_json_types(user_type)
    end

    it 'returns correct values' do
      expect_json(name: params[:name], email: params[:email])
    end
  end

  describe 'POST v1/auth/login' do
    let!(:user) { create(:user, :with_api_key) }
    let(:params) do
      {
        email: user.email,
        password: 'supersecretpassword'
      }
    end

    before { post "#{base_url}/login", params: params }

    it 'returns 201 OK' do
      expect_status(201)
    end

    it 'returns correct types' do
      expect_json_types(api_key_type)
    end

    it 'returns correct values' do
      expect_json(user_id: user.id)
    end
  end

  describe 'POST v1/auth/logout' do
    let(:api_key) { create(:api_key) }
    let(:headers) { { 'Authorization' => api_key.access_token } }

    before { post "#{base_url}/logout", headers: headers }

    it 'returns 200 OK' do
      expect_status(200)
    end

    it 'expires api key' do
      api_key.reload
      expect(api_key.expired?).to be_truthy
    end
  end
end
