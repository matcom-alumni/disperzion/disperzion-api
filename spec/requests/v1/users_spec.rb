# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Users API V1' do
  let(:base_url) { '/v1/users' }

  let(:user_type) do
    {
      id: :integer,
      name: :string,
      email: :string
    }
  end
  let(:user) { create(:user) }
  let!(:users) { create_list(:user, 3) }

  let(:api_key) { create(:api_key, user: user) }
  let(:headers) { { 'Authorization' => api_key.access_token } }

  describe 'GET /users' do
    before { get base_url, headers: headers }

    it 'returns all users' do
      expect_json_sizes(4)
    end

    it 'returns status code 200' do
      expect_status(200)
    end

    it 'returns right fields' do
      expect_json_types('*', user_type)
    end
  end

  describe 'GET /users/:user_id' do
    let(:user) { users.first }

    before { get "#{base_url}/#{user.id}", headers: headers }

    it 'returns status code 200' do
      expect_status(200)
    end

    it 'returns right fields' do
      expect_json_types(user_type)
    end

    context 'when wrong id provided' do
      before { get "#{base_url}/0", headers: headers }

      it 'returns error 404' do
        expect_status(404)
      end
    end
  end

  describe 'POST /users' do
    let(:params) { attributes_for(:user) }

    before { post base_url, params: params, headers: headers }

    it 'returns status code 201' do
      expect_status(201)
    end

    it 'returns right fields' do
      expect_json_types(user_type)
    end

    context 'when invalid params' do
      let(:params) { attributes_for(:user, name: nil) }

      it 'returns status code 422' do
        expect_status(422)
      end
    end
  end

  describe 'PATCH /users/:user_id' do
    let(:params) do
      {
        name: Faker::Name.name,
        email: Faker::Internet.email,
        password: 'secret',
        password_confirmation: 'secret'
      }
    end
    let!(:user) { create(:user) }

    before { patch "#{base_url}/#{user.id}", params: params, headers: headers }

    it 'returns status code 200' do
      expect_status(200)
    end

    it 'returns right fields' do
      expect_json_types(user_type)
    end

    it 'returns right data' do
      expect_json(name: params[:name])
    end
  end

  describe 'DELETE /users/:user_id' do
    let!(:user) { create(:user) }

    it 'returns status code 204' do
      delete "#{base_url}/#{user.id}", headers: headers

      expect_status(204)
    end

    it 'destroys the record' do
      expect {
        delete "#{base_url}/#{user.id}", headers: headers
      }.to change(User, :count).by(-1)
    end
  end

  describe 'GET /user' do
    let(:routing_error_url) { '/V1/user' }
    let(:error_400_url) { '/v1/users/foo' }

    it 'raises RoutingError' do
      expect {
        get routing_error_url, headers: headers
      }.to raise_error(ActionController::RoutingError)
    end

    it 'returns error 400' do
      get error_400_url, headers: headers

      expect_status(400)
    end
  end
end
