# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Verify API V1' do
  let(:base_url) { '/v1/verify' }
  let(:user_type) do
    {
      id: :integer,
      name: :string,
      email: :string
    }
  end
  let(:user) { create(:user, :with_api_key) }
  let(:api_key) { user.api_key }
  let(:headers) { { 'Authorization' => api_key.access_token } }

  describe 'GET /verify' do
    before { get base_url, headers: headers }

    it 'returns status code 200' do
      expect_status(200)
    end

    it 'returns right fields' do
      expect_json_types(user_type)
    end

    it 'returns right values' do
      expect_json(email: user.email,
                  name: user.name)
    end
  end
end
