# frozen_string_literal: true

FactoryBot.define do
  factory :api_key do
    access_token { SecureRandom.hex }
    expires_at { 1.day.from_now }
    association :user
    active { true }
  end
end
