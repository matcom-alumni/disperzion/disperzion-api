# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    password { "supersecretpassword" }
    password_confirmation { password }

    trait :with_api_key do
      association :api_key
    end
  end
end
