# DisperZion

### Abstract

For many years students and graduates of Mathematics and Computer Science from
the University of Havana have been interested in building __MatCom__ community
and there have been many attempts to do it. When Internet arrives to Cuba
there was an excellent try within Telegram called _"MatCom Alumni"_ which has
been an essential part of this group dispersed (this word gave us a name)
throughout the world and in which the idea that we present below arosed.

### Product

**DisperZion** is a web app for making people all over the world be located,
within a map, and this way let other people you know must probably not using social
networks be in touch.

### Objective

For a long time people has speculated that 70% or more of cuban graduates
emigrate due to economic issues. This figure remains uncertain since there is
no way to prove it; but we hope whit _DisperZion_ to provide a sort of
statistical evidence for it.

### _Note_:

This document has been translated from [the
original](https://hackmd.io/T4ijM2zyTgCJPSLFl13JKQ) in Spanish.
