FROM ruby:2.6.6-alpine as development-stage

RUN apk add --no-cache --update \
    build-base \
    gmp-dev \
    postgresql-dev \
    tzdata \
    curl

WORKDIR /app 

COPY Gemfile* ./ 

RUN gem install --no-document bundler:1.17.3 && bundle install

EXPOSE 3000

ENTRYPOINT bundle exec rails

CMD server -b 0.0.0.0 -p 3000