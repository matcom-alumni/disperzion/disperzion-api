class CreateCountryUser < ActiveRecord::Migration[6.0]
  def change
    create_table :country_users do |t|
      t.references :user, index: true, foreign_key: true

      t.string :country_code
    end
  end
end
