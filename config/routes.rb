# frozen_string_literal: true

Rails.application.routes.draw do
  mount GrapeSwaggerRails::Engine => '/docs', as: 'api_docs'
  mount Api::Api => '/', anchor: false
  root to: redirect('/v1/users')
end
